# filepath = 'E:/Repositories/adventofcode_2020/08/test/08Sample.txt'
# filepath = 'E:/Repositories/adventofcode_2020/08/test/08SamplePart2.txt'
filepath = 'E:/Repositories/adventofcode_2020/08/test/08Input.txt'


def IsOp(str):
    if str == 'acc' or str == 'jmp' or str == 'nop':
        return True
    else:
        return False


def IsInt(str):
    try:
        int(str)
        return True
    except BaseException:
        return False


class App:
    def __init__(self):
        self.solutionPart1 = -1
        self.solutionPart2 = -1
        self.input = []
        self.acc = 0

    def Run(self):
        self.Input()
        self.Part1()
        self.Part2()

    def Input(self):
        lines = ''
        with open(filepath, 'r') as f:
            lines = f.readlines()

        for line in lines:
            line = line.strip()
            [op, arg] = line.split()
            assert(IsOp(op))
            assert(IsInt(arg))
            arg = int(arg)
            self.input.append({op: arg})

    def Execute(self, instructions):
        self.acc = 0
        self.freq = [0] * len(instructions)
        index = 0
        while index < len(instructions) and index >= 0:
            op, arg = list(instructions[index].items())[0]
            self.freq[index] += 1
            if self.freq[index] > 1:
                self.solutionPart1 = self.acc
                return False
            if op == 'acc':
                self.acc += arg
                index += 1
                continue
            elif op == 'jmp':
                index += arg
                continue
            elif op == 'nop':
                index += 1
                continue
            assert(False)
        if index == len(instructions):
            return True
        return False

    def Part1(self):
        b = self.Execute(self.input.copy())
        assert(b == False)

    def Part2(self):
        instructionsCopy = self.input.copy()
        nopJmpIndexes = []
        for index in range(0, len(instructionsCopy)):
            op, arg = list(instructionsCopy[index].items())[0]
            if op == 'jmp' or op == 'nop':
                nopJmpIndexes.append(index)

        k = 0
        finished = False
        while(finished == False):
            op, arg = list(instructionsCopy[nopJmpIndexes[k]].items())[0]
            if (op == 'jmp'):
                newop = 'nop'
            elif (op == 'nop'):
                newop = 'jmp'
            instructionsCopy[nopJmpIndexes[k]
                             ][newop] = instructionsCopy[nopJmpIndexes[k]].pop(op)
            finished = self.Execute(instructionsCopy)
            instructionsCopy[nopJmpIndexes[k]
                             ][op] = instructionsCopy[nopJmpIndexes[k]].pop(newop)
            k += 1
            assert(k < len(nopJmpIndexes))

        self.solutionPart2 = self.acc


if __name__ == "__main__":
    app = App()
    app.Run()
    print(app.solutionPart1)
    print(app.solutionPart2)
