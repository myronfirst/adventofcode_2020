#include "App.h"

#include <algorithm>
#include <array>
#include <cassert>
#include <cstdio>
#include <fstream>
#include <functional>
#include <numeric>
#include <sstream>
#include <string>

namespace {
    template<typename T, typename P>
    inline bool FindIf(T begin, T end, P predicate) {
        return (std::find_if(begin, end, predicate) != end);
    }
    template<typename T, typename F>
    inline void ForEach(T begin, T end, F fun) {
        std::for_each(begin, end, fun);
    }
    template<typename T, typename F>
    inline long Count(T begin, T end, F val) {
        return std::count(begin, end, val);
    }
    template<typename T, typename I, typename F>
    inline long long Accumulate(T begin, T end, I init, F operation) {
        return std::accumulate(begin, end, init, operation);
    }
    inline auto FindIfValues(const std::array<char, 1024>& array, const std::string& values) {
        return FindIf(std::cbegin(array), std::cend(array), [&values](char el) -> bool {
            for (const auto& v : values)
                if (el == v) return true;
            return false;
        });
    }
    inline auto CountVec(const std::vector<bool>& vec, bool val) {
        return Count(std::cbegin(vec), std::cend(vec), val);
    }
    inline auto CountStr(const std::string& str, char val) {
        return static_cast<int>(Count(std::cbegin(str), std::cend(str), val));
    }
    inline auto ForEachVec(const std::vector<Step>& vec, const std::function<void(const Step&)>& fun) {
        return ForEach(std::cbegin(vec), std::cend(vec), fun);
    }
    inline auto MultiplyVec(const std::vector<int>& vec) {
        return Accumulate(std::cbegin(vec), std::cend(vec), 1LL, std::multiplies<>());
    }
}    // namespace

int Puzzle::GetIndex(int x, int y) const {
    assert(y < height);
    int mx = x % width;
    int my = y % height;
    int index = my * width + mx;
    return index;
}

App::App(const char* filename) {
    std::ifstream ifs(filename);
    assert(ifs);
    std::string data{};
    int lineSize{ 0 };
    int lines{ 0 };
    for (std::array<char, 1024> buf{}; ifs.getline(buf.data(), buf.size()); buf.fill(0)) {
        if (FindIfValues(buf, "")) continue;
        std::string str{ buf.data() };
        int strSize = static_cast<int>(str.size());
        if (lineSize == 0)
            lineSize = strSize;
        else
            assert(lineSize == strSize);
        ++lines;
        data.append(str);
    }
    puzzle = { data, lineSize, lines };
}

auto App::Solve() -> long long {
    naiveSolution = NaiveSolution();
    auto sol = naiveSolution;
    return sol;
}

auto App::Solve2() -> long long {
    naiveSolution2 = NaiveSolution2();
    auto sol = naiveSolution2;
    return sol;
}

auto App::NaiveSolution() -> long long {
    solution.clear();
    Step s{ 3, 1 };
    int x = 0;
    int y = 0;
    while (y < puzzle.height) {
        char c = puzzle.data.at(puzzle.GetIndex(x, y));
        solution.push_back({ c == HASH });
        x += s.right;
        y += s.down;
    }
    assert(solution.size() > 0);
    return CountVec(solution, true);
}

auto App::NaiveSolution2() -> long long {
    std::vector<int> treesPerSlope{};
    std::vector<Step> vec{ { 1, 1 }, { 3, 1 }, { 5, 1 }, { 7, 1 }, { 1, 2 } };
    ForEachVec(vec, [this, &treesPerSlope](const Step& s) -> void {
        solution.clear();
        int x = 0;
        int y = 0;
        while (y < puzzle.height) {
            char c = puzzle.data.at(puzzle.GetIndex(x, y));
            solution.push_back({ c == HASH });
            x += s.right;
            y += s.down;
        }
        assert(solution.size() > 0);
        treesPerSlope.push_back(static_cast<int>(CountVec(solution, true)));
    });
    return MultiplyVec(treesPerSlope);
}
