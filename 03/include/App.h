#ifndef _APP_H_
#define _APP_H_

// #include <string>
#include <unordered_set>
#include <vector>

struct Puzzle {
    std::string data{};
    int width{};
    int height{};
    int GetIndex(int x, int y) const;
};

struct Step {
    int right{};
    int down{};
};

struct App {
    static constexpr char HASH = '#';
    static constexpr char DOT = '.';

    Puzzle puzzle{};
    std::vector<bool> solution{};
    long naiveSolution = -1;
    long naiveSolution2 = -1;

    App(const char* filename);
    auto NaiveSolution() -> long long;
    auto NaiveSolution2() -> long long;
    auto Solve() -> long long;
    auto Solve2() -> long long;

    ~App() = default;
    App() = delete;
    App(const App&) = delete;
    App& operator=(const App& other) = delete;
    App(App&&) = delete;
    App& operator=(App&& other) = delete;
};

#endif
