# filepath = 'E:/Repositories/adventofcode_2020/06/test/06Sample.txt'
filepath = 'E:/Repositories/adventofcode_2020/06/test/06Input.txt'


class App:
    def __init__(self):
        self.solutionsPart1 = []
        self.solutionsPart2 = []

    def Run(self):
        self.Part1()
        self.Part2()

    def Part1(self):
        lines = ''
        with open(filepath, 'r') as f:
            lines = f.readlines()

        answersList = []
        answers = ''
        for line in lines:
            line = line.strip()
            if not line:
                answersList.append(answers)
                answers = ''
            else:
                answers += line
        if (answers):
            answersList.append(answers)
            answers = ''

        for a in answersList:
            a = ''.join(set(a))
            self.solutionsPart1.append(len(a))

    def Part2(self):
        lines = ''
        with open(filepath, 'r') as f:
            lines = f.readlines()

        groupList = []
        group = []
        for line in lines:
            line = line.strip()
            if not line:
                groupList.append(group)
                group = []
            else:
                group.append(line)
        if (group):
            groupList.append(group)
            group = []

        for group in groupList:
            freq = {}
            for answers in group:
                for a in answers:
                    freq.setdefault(a, 0)
                    freq[a] += 1

            allYes = 0
            for f in freq.values():
                if (f == len(group)):
                    allYes += 1

            self.solutionsPart2.append(allYes)


if __name__ == "__main__":
    app = App()
    app.Run()
    print(app.solutionsPart1)
    print(sum(app.solutionsPart1))
    print(app.solutionsPart2)
    print(sum(app.solutionsPart2))
