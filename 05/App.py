# filepath = 'E:/Repositories/adventofcode_2020/05/test/05Sample.txt'
filepath = 'E:/Repositories/adventofcode_2020/05/test/05Input.txt'

SymbolToHalf = {'F': 'lower', 'B': 'upper', 'L': 'lower', 'R': 'upper', }


def partition(l, r, whatHalf):
    half = (r - l) // 2
    if whatHalf == 'lower':
        return (l, r - half)
    elif whatHalf == 'upper':
        return (l + half, r)
    else:
        assert(False)


class App:
    def __init__(self):
        self.solutionsPart1 = []
        self.solutionsPart2 = []

    def Run(self):
        self.Part1()
        self.Part2()

    def Part1(self):
        lines = ''
        with open(filepath, 'r') as f:
            lines = f.readlines()

        for line in lines:
            line = line.strip()
            rowSymbols = line[:-3]
            colSymbols = line[-3:]
            (l, r) = (0, 128)
            for symbol in rowSymbols:
                (l, r) = partition(l, r, SymbolToHalf[symbol])
            assert(l == r - 1)
            row = l

            (l, r) = (0, 8)
            for symbol in colSymbols:
                (l, r) = partition(l, r, SymbolToHalf[symbol])
            assert(l == r - 1)
            col = l

            seatId = row * 8 + col
            self.solutionsPart1.append(seatId)

        self.solutionsPart1.sort()

    def Part2(self):
        for n in range(self.solutionsPart1[0], self.solutionsPart1[-1] + 1):
            if n not in self.solutionsPart1:
                self.solutionsPart2.append(n)
                return


if __name__ == "__main__":
    app = App()
    app.Run()
    print(app.solutionsPart1)
    print(app.solutionsPart2)
