#ifndef _APP_H_
#define _APP_H_

#include <unordered_set>
#include <vector>

struct Entry {
    int first;
    int second;
    int third = -1;
};

struct App {
    using Pair = std::pair<int, int>;
    using HashSet = std::unordered_set<int>;

    static constexpr int SolutionSum = 2020;    //alias x

    std::vector<int> puzzle{};
    std::vector<Entry> entries{};
    int naiveSolutionSum = -1;
    int sortingSolutionSum = -1;
    int hashingSolutionSum = -1;
    int remainderSolutionSum = -1;
    int naiveSolution2Sum = -1;

    HashSet hashSet{};

    App(const char* filename);
    auto NaiveSolution() -> int;        //Time O(n^2), Space O(n)
    auto SortingSolution() -> int;      //Time O(O(std::sort)), Space O(O(std::sort))
    auto HashingSolution() -> int;      //Time O(n), Space O(n)
    auto RemainderSolution() -> int;    //Time O(n+x), Space O(x)
    auto NaiveSolution2() -> int;       //Time O(n^3), Space O(n)
    auto Solve() -> int;
    auto Solve2() -> int;

    ~App() = default;
    App() = delete;
    App(const App&) = delete;
    App& operator=(const App& other) = delete;
    App(App&&) = delete;
    App& operator=(App&& other) = delete;
};

#endif
