#include "App.h"

#include <algorithm>
#include <array>
#include <cassert>
#include <fstream>
#include <functional>
#include <string>

namespace {
    template<typename T, typename F>
    inline void ForEach(T begin, T end, F fun) {
        std::for_each(begin, end, fun);
    }
    template<typename T, typename P>
    inline bool FindIf(T begin, T end, P predicate) {
        return (std::find_if(begin, end, predicate) != end);
    }
    inline auto FindIfValues(const std::array<char, 1024>& array, const std::string& values) {
        return FindIf(std::cbegin(array), std::cend(array), [&values](char el) -> bool {
            for (const auto& v : values)
                if (el == v) return true;
            return false;
        });
    }

}    // namespace

App::App(const char* filename) {
    std::ifstream ifs(filename);
    assert(ifs);
    for (std::array<char, 1024> buf{}; ifs.getline(buf.data(), buf.size()); buf.fill(0)) {
        if (FindIfValues(buf, "#!")) continue;
        puzzle.push_back(std::stoi(buf.data()));
    }
}

auto App::Solve() -> int {
    naiveSolutionSum = NaiveSolution();
    sortingSolutionSum = SortingSolution();
    hashingSolutionSum = HashingSolution();
    remainderSolutionSum = RemainderSolution();
    assert((naiveSolutionSum == sortingSolutionSum) && (sortingSolutionSum == hashingSolutionSum) && (hashingSolutionSum == remainderSolutionSum));
    auto sol = naiveSolutionSum;
    return sol;
}

auto App::Solve2() -> int {
    naiveSolution2Sum = NaiveSolution2();
    auto sol = naiveSolution2Sum;
    return sol;
}

auto App::NaiveSolution() -> int {
    entries.clear();
    for (const auto& i : puzzle)
        for (const auto& j : puzzle)
            if (i + j == 2020)
                entries.push_back({ i, j });
    assert(entries.size() > 0);
    return (entries.at(0).first * entries.at(0).second);
}

auto App::SortingSolution() -> int {
    entries.clear();
    auto copy = puzzle;
    std::sort(std::begin(copy), std::end(copy), std::less<int>());
    auto it1 = std::begin(copy);
    auto it2 = std::end(copy) - 1;
    while (it1 < it2) {
        auto sum = (*it1) + (*it2);
        if (sum == SolutionSum) {
            entries.push_back({ *it1, *it2 });
            ++it1;
            --it2;
        } else if (sum < SolutionSum)
            ++it1;
        else if (sum > SolutionSum)
            --it2;
        else
            assert(false);
    }
    assert(entries.size() > 0);
    return (entries.at(0).first * entries.at(0).second);
}

auto App::HashingSolution() -> int {
    entries.clear();
    ForEach(std::cbegin(puzzle), std::cend(puzzle), [this](int key) -> void {
        int diffKey = SolutionSum - key;
        if (hashSet.count(diffKey) == 0)
            hashSet.insert(key);
        else
            entries.push_back({ key, diffKey });
    });
    assert(entries.size() > 0);
    return (entries.at(0).first * entries.at(0).second);
}

auto App::RemainderSolution() -> int {
    entries.clear();
    std::array<int, SolutionSum> modArray;
    modArray.fill(0);
    ForEach(std::cbegin(puzzle), std::cend(puzzle), [&modArray](int num) -> void {
        int key = num % SolutionSum;
        modArray.at(key) += 1;
    });
    int half = SolutionSum / 2;
    for (int i = 0; i < half; ++i) {
        int diff = SolutionSum - i;
        if (modArray.at(i) && modArray.at(diff))
            entries.push_back({ i, diff });
    }
    if (half % 2 == 0) {
        if (modArray.at(half) > 1)
            entries.push_back({ half, half });
    } else {
        if (modArray.at(half) && modArray.at(SolutionSum - half))
            entries.push_back({ half, SolutionSum - half });
    }
    assert(entries.size() > 0);
    return (entries.at(0).first * entries.at(0).second);
}

auto App::NaiveSolution2() -> int {
    entries.clear();
    for (const auto& i : puzzle)
        for (const auto& j : puzzle)
            for (const auto& k : puzzle)
                if (i + j + k == 2020)
                    entries.push_back({ i, j, k });
    assert(entries.size() > 0);
    return (entries.at(0).first * entries.at(0).second * entries.at(0).third);
}
