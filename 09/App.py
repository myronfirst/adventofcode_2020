# filepath = 'E:/Repositories/adventofcode_2020/09/test/09Sample.txt'
# preambleLen = 5
filepath = 'E:/Repositories/adventofcode_2020/09/test/09Input.txt'
preambleLen = 25


def IsInt(str):
    try:
        int(str)
        return True
    except BaseException:
        return False


class App:
    def __init__(self):
        self.solutionPart1 = -1
        self.solutionPart2 = -1
        self.input = []

    def Run(self):
        self.Input()
        self.Part1()
        self.Part2()

    def Input(self):
        lines = ''
        with open(filepath, 'r') as f:
            lines = f.readlines()

        for line in lines:
            line = line.strip()
            assert(IsInt(line))
            self.input.append(int(line))

    def Part1(self):
        for numIndex in range(preambleLen, len(self.input)):
            prevNums = self.input[numIndex - preambleLen: numIndex].copy()
            prevNums = sorted(prevNums)
            next = self.input[numIndex]

            i, j = 0, preambleLen - 1
            sumIsNext = False
            while i < j and sumIsNext == False:
                s = prevNums[i] + prevNums[j]
                if s < next:
                    i += 1
                elif s > next:
                    j -= 1
                else:
                    sumIsNext = True
            if sumIsNext == False:
                self.solutionPart1 = next
                break

    def Part2(self):
        key = self.solutionPart1
        for setSize in range(2, len(self.input)):
            first, last = 0, setSize - 1
            while last < len(self.input):
                numSet = self.input[first: last + 1]
                s = sum(numSet)
                if s != key:
                    first += 1
                    last += 1
                else:
                    self.solutionPart2 = min(numSet) + max(numSet)
                    return


if __name__ == "__main__":
    app = App()
    app.Run()
    print(app.solutionPart1)
    print(app.solutionPart2)
