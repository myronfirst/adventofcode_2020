#ifndef _APP_H_
#define _APP_H_

#include <functional>
#include <string>
#include <unordered_map>
#include <vector>

struct App {
    using Passport = std::unordered_map<std::string, std::string>;
    using IsValidFieldDispatcher = std::unordered_map<std::string, std::function<bool(const Passport&)>>;

    std::vector<Passport> puzzle{};
    IsValidFieldDispatcher isValidFieldDispatcher{};
    int naiveSolution = -1;
    int naiveSolution2 = -1;

    void InitializeDispatcher();
    App(const std::string& filename);
    auto NaiveSolution() -> int;
    auto NaiveSolution2() -> int;
    auto Solve() -> int;
    auto Solve2() -> int;

    ~App() = default;
    App() = delete;
    App(const App&) = delete;
    App& operator=(const App& other) = delete;
    App(App&&) = delete;
    App& operator=(App&& other) = delete;
};

#endif
