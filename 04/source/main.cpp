#include "App.h"

#include <iostream>
#include <string>

constexpr char fileName[] = "input.txt";

int main() {
    App app{ fileName };
    std::cout << app.Solve() << std::endl;
    std::cout << app.Solve2() << std::endl;
    return 0;
}
