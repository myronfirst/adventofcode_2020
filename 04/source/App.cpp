#include "App.h"

#include <algorithm>
#include <array>
#include <cassert>
#include <cstdio>
#include <exception>
#include <fstream>
#include <functional>
#include <limits>
#include <numeric>
#include <sstream>
#include <string>

namespace {
    template<typename T, typename P>
    inline bool FindIf(T begin, T end, P predicate) {
        return (std::find_if(begin, end, predicate) != end);
    }
    template<typename T, typename F>
    inline void ForEach(T begin, T end, F fun) {
        std::for_each(begin, end, fun);
    }
    template<typename T, typename V>
    inline long Count(T begin, T end, V val) {
        return std::count(begin, end, val);
    }
    template<typename T, typename P>
    inline long CountIf(T begin, T end, P predicate) {
        return std::count_if(begin, end, predicate);
    }
    template<typename T, typename I, typename F>
    inline long long Accumulate(T begin, T end, I init, F operation) {
        return std::accumulate(begin, end, init, operation);
    }
    inline auto FindIfValues(const std::array<char, 1024>& array, const std::string& values) {
        return FindIf(std::cbegin(array), std::cend(array), [&values](char el) -> bool {
            for (const auto& v : values)
                if (el == v) return true;
            return false;
        });
    }
    inline auto CountVec(const std::vector<bool>& vec, bool val) {
        return static_cast<int>(Count(std::cbegin(vec), std::cend(vec), val));
    }
    inline auto CountIfVec(const std::vector<App::Passport>& vec, const std::function<bool(const App::Passport&)>& fun) {
        return CountIf(std::cbegin(vec), std::cend(vec), fun);
    }
    inline auto CountIfStr(const std::string& str, const std::function<bool(const char&)>& fun) {
        return static_cast<int>(CountIf(std::cbegin(str), std::cend(str), fun));
    }
    inline int STOI(const std::string& s) {
        int min = std::numeric_limits<int>::min();
        int n = min;
        try {
            n = stoi(s);
        } catch (const std::exception& e) { return min; }
        return n;
    }

    inline bool BYR_Rules(const App::Passport& pass) {
        if (pass.count("byr") == 0) return false;
        std::string val{ pass.at("byr") };
        if (val.size() != 4) return false;
        int n = STOI(val);
        if (n < 1920 || n > 2002) return false;
        return true;
    }
    inline bool IYR_Rules(const App::Passport& pass) {
        if (pass.count("iyr") == 0) return false;
        std::string val{ pass.at("iyr") };
        if (val.size() != 4) return false;
        int n = STOI(val);
        if (n < 2010 || n > 2020) return false;
        return true;
    }
    inline bool EYR_Rules(const App::Passport& pass) {
        if (pass.count("eyr") == 0) return false;
        std::string val{ pass.at("eyr") };
        if (val.size() != 4) return false;
        int n = STOI(val);
        if (n < 2020 || n > 2030) return false;
        return true;
    }
    inline bool HGT_Rules(const App::Passport& pass) {
        if (pass.count("hgt") == 0) return false;
        std::string val{ pass.at("hgt") };
        size_t s = val.size();
        std::string u = val.substr(s - 2);
        bool cm = (u == "cm");
        bool in = (u == "in");
        if ((cm || in) == false) return false;
        std::string numStr{ val, 0, s - 2 };
        int n = STOI(numStr);
        if (cm && (n < 150 || n > 193)) return false;
        if (in && (n < 59 || n > 76)) return false;
        return true;
    }
    inline bool HCL_Rules(const App::Passport& pass) {
        if (pass.count("hcl") == 0) return false;
        std::string val{ pass.at("hcl") };
        if (val.at(0) != '#') return false;
        if (val.size() != 7) return false;
        std::string color{ val, 1, val.size() };
        int num = CountIfStr(color, [](const char& c) -> bool {
            return ((c >= '0' && c <= '9') || (c >= 'a' && c <= 'f'));
        });
        return (num == 6);
    }
    inline bool ECL_Rules(const App::Passport& pass) {
        if (pass.count("ecl") == 0) return false;
        std::string val{ pass.at("ecl") };
        if (val.size() != 3) return false;
        bool b = (val == "amb") ||
                 (val == "blu") ||
                 (val == "brn") ||
                 (val == "gry") ||
                 (val == "grn") ||
                 (val == "hzl") ||
                 (val == "oth");
        return b;
    }
    inline bool PID_Rules(const App::Passport& pass) {
        if (pass.count("pid") == 0) return false;
        std::string val{ pass.at("pid") };
        if (val.size() != 9) return false;
        int num = CountIfStr(val, [](const char& c) -> bool {
            return (c >= '0' && c <= '9');
        });
        return (num == 9);
    }
    inline bool CID_Rules(const App::Passport& pass) {
        (void)pass;    //suppress unused warning
        return true;
    }
}    // namespace

void App::InitializeDispatcher() {
    isValidFieldDispatcher.insert({ "byr", [](const Passport& pass) -> bool { return BYR_Rules(pass); } });
    isValidFieldDispatcher.insert({ "iyr", [](const Passport& pass) -> bool { return IYR_Rules(pass); } });
    isValidFieldDispatcher.insert({ "eyr", [](const Passport& pass) -> bool { return EYR_Rules(pass); } });
    isValidFieldDispatcher.insert({ "hgt", [](const Passport& pass) -> bool { return HGT_Rules(pass); } });
    isValidFieldDispatcher.insert({ "hcl", [](const Passport& pass) -> bool { return HCL_Rules(pass); } });
    isValidFieldDispatcher.insert({ "ecl", [](const Passport& pass) -> bool { return ECL_Rules(pass); } });
    isValidFieldDispatcher.insert({ "pid", [](const Passport& pass) -> bool { return PID_Rules(pass); } });
    isValidFieldDispatcher.insert({ "cid", [](const Passport& pass) -> bool { return CID_Rules(pass); } });
}

App::App(const std::string& filename) {
    InitializeDispatcher();
    std::ifstream ifs(filename);
    assert(ifs);
    Passport passport{};
    for (std::array<char, 1024> buf{}; ifs.getline(buf.data(), buf.size()); buf.fill(0)) {
        std::istringstream iss{ std::string(buf.data()) };
        while (true) {
            int issPeek = iss.peek();
            if (issPeek == EOF || issPeek == '\0' || issPeek == '\n') break;
            std::string key{};
            std::string value{};
            std::getline(iss, key, ':');
            std::getline(iss, value, ' ');
            assert(passport.find(key) == passport.end());
            passport.insert({ key, value });
        }
        int ifsPeek = ifs.peek();
        if (ifsPeek == EOF || ifsPeek == '\0' || ifsPeek == '\n') {
            puzzle.push_back(passport);
            passport.clear();
        }
    }
}

auto App::Solve() -> int {
    naiveSolution = NaiveSolution();
    auto sol = naiveSolution;
    return sol;
}

auto App::Solve2() -> int {
    naiveSolution2 = NaiveSolution2();
    auto sol = naiveSolution2;
    return sol;
}

auto App::NaiveSolution() -> int {
    return static_cast<int>(CountIfVec(puzzle, [](const Passport& passport) -> bool {
        unsigned long count =
            passport.count("byr") +
            passport.count("iyr") +
            passport.count("eyr") +
            passport.count("hgt") +
            passport.count("hcl") +
            passport.count("ecl") +
            passport.count("pid");
        return (count >= 7);
    }));
}

auto App::NaiveSolution2() -> int {
    return static_cast<int>(CountIfVec(puzzle, [this](const Passport& passport) -> bool {
        for (const auto& [key, val] : isValidFieldDispatcher) {
            (void)val;    //suppress unused warning
            if (isValidFieldDispatcher.at(key)(passport) == false) return false;
        }
        return true;
    }));
}
