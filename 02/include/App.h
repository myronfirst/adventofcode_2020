#ifndef _APP_H_
#define _APP_H_

#include <string>
#include <unordered_set>
#include <vector>

struct Entry {
    const int min;
    const int max;
    const char symbol;
    const std::string pass;
};

struct App {
    static constexpr int SolutionSum = 2020;

    std::vector<Entry> puzzle{};
    std::vector<bool> solution{};
    int naiveSolutionSum = -1;
    int naiveSolution2Sum = -1;

    App(const char* filename);
    auto NaiveSolution() -> int;
    auto NaiveSolution2() -> int;
    auto Solve() -> int;
    auto Solve2() -> int;

    ~App() = default;
    App() = delete;
    App(const App&) = delete;
    App& operator=(const App& other) = delete;
    App(App&&) = delete;
    App& operator=(App&& other) = delete;
};

#endif
