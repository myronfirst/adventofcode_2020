#include "App.h"

#include <algorithm>
#include <array>
#include <cassert>
#include <cstdio>
#include <fstream>
#include <functional>
#include <sstream>
#include <string>

namespace {
    template<typename T, typename P>
    inline bool FindIf(T begin, T end, P predicate) {
        return (std::find_if(begin, end, predicate) != end);
    }
    template<typename T, typename F>
    inline void ForEach(T begin, T end, F fun) {
        std::for_each(begin, end, fun);
    }
    template<typename T, typename F>
    inline long Count(T begin, T end, F val) {
        return std::count(begin, end, val);
    }
    inline auto FindIfValues(const std::array<char, 1024>& array, const std::string& values) {
        return FindIf(std::cbegin(array), std::cend(array), [&values](char el) -> bool {
            for (const auto& v : values)
                if (el == v) return true;
            return false;
        });
    }
    inline auto ForEachVec(const std::vector<Entry>& vec, const std::function<void(const Entry& entry)>& fun) {
        return ForEach(std::cbegin(vec), std::cend(vec), fun);
    }
    inline auto CountVec(const std::vector<bool>& vec, bool val) {
        return static_cast<int>(Count(std::cbegin(vec), std::cend(vec), val));
    }
    inline auto CountStr(const std::string& str, char val) {
        return static_cast<int>(Count(std::cbegin(str), std::cend(str), val));
    }
}    // namespace

App::App(const char* filename) {
    std::ifstream ifs(filename);
    assert(ifs);
    for (std::array<char, 1024> buf{}; ifs.getline(buf.data(), buf.size()); buf.fill(0)) {
        if (FindIfValues(buf, "#!")) continue;
        int min, max;
        char symbol;
        char pass[1024];
        sscanf(buf.data(), "%d-%d %c: %s\n", &min, &max, &symbol, pass);
        Entry e{ min, max, symbol, std::string(pass) };
        puzzle.push_back(e);
    }
}

auto App::Solve() -> int {
    naiveSolutionSum = NaiveSolution();
    auto sol = naiveSolutionSum;
    return sol;
}

auto App::Solve2() -> int {
    naiveSolution2Sum = NaiveSolution2();
    auto sol = naiveSolution2Sum;
    return sol;
}

auto App::NaiveSolution() -> int {
    solution.clear();
    ForEachVec(puzzle, [this](const Entry& e) -> void {
        int count = CountStr(e.pass, e.symbol);
        solution.push_back({ (count >= e.min) && (count <= e.max) });
    });
    assert(solution.size() > 0);
    return CountVec(solution, true);
}

auto App::NaiveSolution2() -> int {
    solution.clear();
    ForEachVec(puzzle, [this](const Entry& e) -> void {
        bool isSymbolA = e.pass.at(e.min - 1) == e.symbol;
        bool isSymbolB = e.pass.at(e.max - 1) == e.symbol;
        solution.push_back({ isSymbolA != isSymbolB });
    });
    assert(solution.size() > 0);
    return CountVec(solution, true);
}
