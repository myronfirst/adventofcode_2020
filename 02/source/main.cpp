#include "App.h"

#include <iostream>

int main() {
    const char* fileName = "input.txt";
    App app{ fileName };
    std::cout << app.Solve() << std::endl;
    std::cout << app.Solve2() << std::endl;
    return 0;
}
