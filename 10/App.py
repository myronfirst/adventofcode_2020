# filepath = 'E:/Repositories/adventofcode_2020/10/test/10Sample.txt'
filepath = 'E:/Repositories/adventofcode_2020/10/test/10Input.txt'


def IsInt(str):
    try:
        int(str)
        return True
    except BaseException:
        return False


class App:
    def __init__(self):
        self.solutionPart1 = -1
        self.solutionPart2 = -1
        self.input = []

    def Run(self):
        self.Input()
        self.Part1()
        self.Part2()

    def Input(self):
        lines = ''
        with open(filepath, 'r') as f:
            lines = f.readlines()

        for line in lines:
            line = line.strip()
            assert(IsInt(line))
            self.input.append(int(line))

    def Part1(self):
        jolts = sorted(self.input.copy())
        jolts.append(jolts[len(jolts) - 1] + 3)
        difs = {}
        prev = 0
        for curr in jolts:
            dif = curr - prev
            if dif not in difs:
                difs[dif] = 0
            difs[dif] += 1
            prev = curr
            assert(dif <= 3)
        print(difs)
        self.solutionPart1 = difs[1] * difs[3]

    def Part2(self):
        jolts = sorted(self.input.copy())
        jolts.append(jolts[-1] + 3)
        paths_to_reach = {0: 1}
        for val in jolts:
            paths_to_reach[val] = paths_to_reach.get(val - 3, 0) + \
                paths_to_reach.get(val - 2, 0) + paths_to_reach.get(val - 1, 0)
        self.solutionPart2 = paths_to_reach.get(jolts[-1])


if __name__ == "__main__":
    app = App()
    app.Run()
    print(app.solutionPart1)
    print(app.solutionPart2)
