import copy
import functools

# filepath = 'E:/Repositories/adventofcode_2020/11/test/11Test.txt'
# filepath = 'E:/Repositories/adventofcode_2020/11/test/11Sample.txt'
filepath = 'E:/Repositories/adventofcode_2020/11/test/11Input.txt'


class Seats:
    def __init__(self, input):
        for line in input:
            assert(len(line) == len(input[-1]))

        self.rows = len(input)
        self.cols = len(input[0])
        self.data_rows = len(input) + 2
        self.data_cols = len(input[0]) + 2
        self.data = []

        self.data += (self.data_cols) * ['.']
        for line in input:
            self.data.append('.')
            self.data += line
            self.data.append('.')
        self.data += (self.data_cols) * ['.']

    def copy(self):
        return copy.deepcopy(self)

    def count(self, val):
        assert(val == '#' or val == 'L' or val == '.')
        return self.data.count(val)

    def set(self, i, j, val):
        assert(val == '#' or val == 'L')
        self.data[self.data_cols * i + j] = val

    def get(self, i, j):
        return self.data[self.data_cols * i + j]

    def get_occupied_adjacent(self, i, j):
        assert(self.get(i, j) != '.')
        adj = [self.get(i - 1, j - 1),
               self.get(i - 1, j),
               self.get(i - 1, j + 1),
               self.get(i, j - 1),
               #    self.get(i, j),
               self.get(i, j + 1),
               self.get(i + 1, j - 1),
               self.get(i + 1, j),
               self.get(i + 1, j + 1)
               ]
        return adj.count('#')

    def get_occupied(self, i, j, part2=False):
        count = 0
        for x in (-1, 0, 1):
            for y in (-1, 0, 1):
                if x == 0 and y == 0:
                    continue
                r = i + x
                c = j + y
                while (part2 and 0 < r <= self.rows and
                       0 < c <= self.cols and self.get(r, c) == '.'):
                    r += x
                    c += y
                if (self.get(r, c) == '#'):
                    count += 1
        return count

    def rdc(self, iterable):
        return functools.reduce(lambda acc, val: acc if acc != '.' else val if val != '.' else '.',
                                iterable, '.')

    def diagonal(self, i, j, dir):
        dir_to_increment = {'north-west': (-1, -1),
                            'north-east': (-1, 1),
                            'south-east': (1, 1),
                            'south-west': (1, -1), }
        ret = []
        while True:
            inc = dir_to_increment.get(dir)
            i += inc[0]
            j += inc[1]
            if (i < 1 or j < 1 or i > self.rows or j > self.cols):
                break
            ret.append(self.get(i, j))

        return ret

    def get_occupied_all_directions(self, i, j):
        assert(self.get(i, j) != '.')
        r_b, c_b = 1, 1
        r_e, c_e = self.rows, self.cols
        adj = [self.rdc(self.diagonal(i, j, 'north-west')),  # north-west
               self.rdc([self.get(k, j)
                        for k in range(i - 1, r_b - 1, -1)]),  # north
               self.rdc(self.diagonal(i, j, 'north-east')),  # north-east
               self.rdc([self.get(i, k)
                        for k in range(j + 1, c_e + 1, 1)]),  # east
               self.rdc(self.diagonal(i, j, 'south-east')),  # south-east
               self.rdc([self.get(k, j)
                        for k in range(i + 1, r_e + 1, 1)]),  # south
               self.rdc(self.diagonal(i, j, 'south-west')),  # south-west
               self.rdc([self.get(i, k)
                        for k in range(j - 1, c_b - 1, -1)])  # west
               ]
        return adj.count('#')

    def is_stabilized(self, part2=False):
        for i in range(1, self.rows + 1):
            for j in range(1, self.cols + 1):
                status = self.get(i, j)
                if (status == '.'):
                    continue
                num_occupied = (
                    self.get_occupied_adjacent(
                        i, j) if not part2 else self.get_occupied_all_directions(
                        i, j))
                if status == 'L' and num_occupied == 0:
                    return False
                elif status == '#' and num_occupied >= (4 if not part2 else 5):
                    return False
        return True

    def print(self):
        out = []
        for i in range(0, len(self.data)):
            if i % self.data_cols == 0:
                out.append('\n')
            out.append(self.data[i])
        out.append('\n')
        with open("out.txt", 'a') as f:
            f.write(''.join(out))


class App:
    def __init__(self):
        self.solutionPart1 = -1
        self.solutionPart2 = -1
        self.seats = None

    def Run(self):
        self.Input()
        self.Part1()
        self.Part2()

    def Input(self):
        lines = ''
        with open(filepath, 'r') as f:
            lines = f.readlines()

        input = []
        for line in lines:
            line = line.strip()
            input.append(line)
        self.seats = Seats(input)

    def Part1(self):
        seats = self.seats.copy()
        while True:
            done = True
            buffer_seats = seats.copy()
            # seats.print()
            for i in range(1, seats.rows + 1):
                for j in range(1, seats.cols + 1):
                    status = buffer_seats.get(i, j)
                    if (status == '.'):
                        continue
                    num_occupied = buffer_seats.get_occupied(i, j)
                    if status == 'L' and num_occupied == 0:
                        seats.set(i, j, '#')
                        if done:
                            done = False
                    elif status == '#' and num_occupied >= 4:
                        seats.set(i, j, 'L')
                        if done:
                            done = False
            if done:
                break
        # seats.print()
        assert(seats.is_stabilized())
        self.solutionPart1 = seats.count('#')

    def Part2(self):
        seats = self.seats.copy()
        while True:
            done = True
            buffer_seats = seats.copy()
            # seats.print()
            for i in range(1, seats.rows + 1):
                for j in range(1, seats.cols + 1):
                    status = buffer_seats.get(i, j)
                    if (status == '.'):
                        continue
                    # num_occupied = buffer_seats.get_occupied_all_directions(i, j)
                    num_occupied = buffer_seats.get_occupied(i, j, part2=True)
                    if status == 'L' and num_occupied == 0:
                        seats.set(i, j, '#')
                        if done:
                            done = False
                    elif status == '#' and num_occupied >= 5:  # changed in part2
                        seats.set(i, j, 'L')
                        if done:
                            done = False
            if done:
                break
            # seats.print()

        assert(seats.is_stabilized(part2=True))
        self.solutionPart2 = seats.count('#')


if __name__ == "__main__":
    app = App()
    app.Run()
    print(app.solutionPart1)
    print(app.solutionPart2)
