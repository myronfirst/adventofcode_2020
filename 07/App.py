# filepath = 'E:/Repositories/adventofcode_2020/07/test/07Sample.txt'
# filepath = 'E:/Repositories/adventofcode_2020/07/test/07SamplePart2.txt'
filepath = 'E:/Repositories/adventofcode_2020/07/test/07Input.txt'


def TotalBagsDFS(colorKey, map, sumList):
    colorKeySum = 1
    for pair in map.get(colorKey):
        num = pair.get('num')
        color = pair.get('color')
        colorKeySum += num * TotalBagsDFS(color, map, sumList)
    sumList.append(colorKeySum)
    return colorKeySum


class App:
    def __init__(self):
        self.inputMap = {}
        self.solutionsPart1 = []
        self.solutionsPart2 = []

    def Run(self):
        self.Input()
        self.Part1()
        self.Part2()

    def Input(self):
        lines = ''
        with open(filepath, 'r') as f:
            lines = f.readlines()

        for line in lines:
            line = line.strip()
            [begin, middle, end] = line.partition('contain')
            colorKey = begin.replace('bags', '').strip().replace(' ', '_')
            end = end.replace('bags', '').replace('bag', '').replace(
                '.', '').strip()

            if(end.find('no other') >= 0):
                self.inputMap.update({colorKey: []})
                continue

            valueList = []
            for s in end.split(','):
                s = s.strip().replace(' ', '_')
                assert(s[0].isdigit())
                num = int(s[0])
                color = s[2:]
                valueList.append({'num': num, 'color': color})
            self.inputMap.update({colorKey: valueList})

    def Part1(self):
        goldChildren = set()
        for pair in self.inputMap.get('shiny_gold'):
            color = pair.get('color')
            goldChildren.add(color)

        reducedInputMap = self.inputMap.copy()
        for discardKey in goldChildren:
            reducedInputMap.pop(discardKey)

        totalParentColors = set()
        parentColors = {'shiny_gold'}
        while (len(parentColors) > 0):
            newParents = []
            for parentColor in parentColors:
                for colorKey, valueList in reducedInputMap.items():
                    for pair in valueList:
                        color = pair.get('color')
                        if (color == parentColor):
                            newParents.append(colorKey)
            newParentsUnique = set(newParents)
            totalParentColors.update(newParentsUnique)
            parentColors = newParentsUnique

        assert(('shiny_gold' in totalParentColors) == False)
        for c in goldChildren:
            assert((c in totalParentColors) == False)

        self.solutionsPart1 = totalParentColors.copy()

    def Part2(self):
        totalParentColors = self.solutionsPart1.copy()
        reducedInputMap = self.inputMap.copy()
        for discardKey in totalParentColors:
            reducedInputMap.pop(discardKey)

        sumList = []
        TotalBagsDFS('shiny_gold', reducedInputMap, sumList)
        sumList[-1] -= 1  # Don't count the gold bag itself
        self.solutionsPart2 = sumList.copy()


if __name__ == "__main__":
    app = App()
    app.Run()
    print(app.solutionsPart1)
    print(len(app.solutionsPart1))
    print(app.solutionsPart2)
    print(max(app.solutionsPart2))
