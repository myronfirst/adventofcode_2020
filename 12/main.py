import math
# PATH = 'E:/Repositories/adventofcode_2020/12/test/12Sample.txt'
PATH = 'E:/Repositories/adventofcode_2020/12/test/12Input.txt'

MAP_SYM = {'N': 0,
           'E': 1,
           'S': 2,
           'W': 3,
           'L': -1,
           'R': -2,
           'F': -3}


def main():
    lines = ''
    with open(PATH, 'r') as f:
        lines = f.readlines()
    directions = []
    for line in lines:
        line = line.strip()
        directions.append((MAP_SYM.get(line[0]), int(line[1:])))

    def part1():
        hor, ver, rot = 0, 0, 1
        for dir in directions:
            _sym, _val = (dir[0], dir[1])

            def operate(sym, val):
                if sym == 0:
                    return 0, +val, 0
                elif sym == 1:
                    return +val, 0, 0
                elif sym == 2:
                    return 0, -val, 0
                elif sym == 3:
                    return -val, 0, 0
                elif sym == -1:
                    val = (val // 90)
                    return 0, 0, - val
                elif sym == -2:
                    val = (val // 90)
                    return 0, 0, + val
                elif sym == -3:
                    return operate(rot, val)
                else:
                    assert(False)

            incHor, incVer, incRot = operate(_sym, _val)
            hor += incHor
            ver += incVer
            rot = (rot + incRot) % 4

        manhattan = abs(hor) + abs(ver)
        print(manhattan)
    part1()

    def rotate(a, b, r):
        return \
            round(a * math.cos(r) - b * math.sin(r)), \
            round(a * math.sin(r) + b * math.cos(r))

    def part2():
        x, y = 0, 0
        p_x, p_y = 10, 1
        for dir in directions:
            sym, val = (dir[0], dir[1])
            if sym == 0:
                p_y += val
            elif sym == 1:
                p_x += val
            elif sym == 2:
                p_y -= val
            elif sym == 3:
                p_x -= val
            elif sym == -1:
                val = math.radians(val)
                p_x, p_y = rotate(p_x, p_y, val)
            elif sym == -2:
                val = math.radians(-val)
                p_x, p_y = rotate(p_x, p_y, val)
            elif sym == -3:
                x += val * p_x
                y += val * p_y
            else:
                assert(False)

        manhattan = abs(x) + abs(y)
        print(manhattan)
    part2()


if __name__ == "__main__":
    main()
